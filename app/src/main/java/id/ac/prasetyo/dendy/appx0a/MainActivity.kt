package id.ac.prasetyo.dendy.appx0a


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMahasiswa ->{
                val intent = Intent(this, MhsActivity::class.java)
                startActivity(intent)
            }
            R.id.btnProdi ->{
                val intent = Intent(this, ProdiActivity::class.java)
                startActivity(intent)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnMahasiswa.setOnClickListener(this)
        btnProdi.setOnClickListener(this)

    }







}
